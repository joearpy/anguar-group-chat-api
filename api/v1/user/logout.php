<?php

session_start();

unset($_SESSION);

session_destroy();

$response = ['success' => true];

echo json_encode($response);