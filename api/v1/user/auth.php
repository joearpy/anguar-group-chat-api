<?php
session_start();

$_POST = json_decode(file_get_contents('php://input'), true);

if (!isset($_POST) || empty($_POST)) {
    $response = ['success' => false, 'secret' => "Only POST accepted"];
    echo json_encode($response);
    return;
}

require_once '../classes/Database.php';
$db = new Database();
$username = $_POST['username'];

if ( !$db->is_user_exists($username) ) {
    $response = ['success' => false, 'secret' => "Invalid credentials"];
    echo json_encode($response);
    return;
}

$_SESSION['user'] = $username;
$response = ['success' => true, 'secret' => "Authentication was successful", 'authenticated_user' => $username];
echo json_encode($response);