<?php

class Database {

    private PDO $pdo;
    private const HOST = "localhost";
    private const DB_NAME = "angular_chat";
    private const USERNAME = "root";
    private const PASS = "";

    public function __construct () {
        try {
            $this->pdo = new PDO("mysql:host=" . self::HOST . ";dbname=". self::DB_NAME, self::USERNAME, self::PASS);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function get_user_by_nickname( string $username ) : array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE username = ?');
        $stmt->execute([$username]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function is_user_exists( string $username ) : bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE username = ?');
        $stmt->execute([$username]);
        if ( $stmt->fetch(PDO::FETCH_ASSOC) ) {
            return true;
        }
        return false;
    }

    public function get_all_messages() : array
    {
        $stmt = $this->pdo->prepare('SELECT m.message, m.creation_time, u.username FROM messages m INNER JOIN users u ON m.user_id = u.id ORDER BY creation_time DESC');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function save_message( int $user_id, string $message ) : void
    {
        $stmt = $this->pdo->prepare('INSERT INTO messages ( `user_id`, `message` ) VALUES (?,?)');
        $stmt->execute([$user_id, $message]);
    }

    public function load_previous_messages ( int $limit ) : array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM messages ORDER BY creation_time DESC LIMIT :limit');
        $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}