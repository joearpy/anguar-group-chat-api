<?php
session_start();

$_POST = json_decode(file_get_contents('php://input'), true);

if (!isset($_POST) || empty($_POST)) {
    $response = ['success' => false, 'secret' => "Only POST accepted"];
    echo json_encode($response);
    return;
}

require_once '../classes/Database.php';
$db = new Database();
$username = $_POST['userName'];
$message = $_POST['message'];

$user_id = $db->get_user_by_nickname($username)['id'] ?? NULL;

if(is_null($user_id)) {
    $response = ['success' => false, 'secret' => "Something went wrong"];
    echo json_encode($response);
    return;
}

$db->save_message( $user_id, $message );

$response = ['success' => true, 'secret' => "Message has been saved"];
echo json_encode($response);